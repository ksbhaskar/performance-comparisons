#!/bin/bash
redis-server --daemonize yes
$ydb_dist/yottadb -run %XCMD 'do start^%zmgsi(0)'
$ydb_dist/yottadb -run %ydbwebreq --port 9080 &
# Needed so that the GUI won't pick up the index.html from the current directory
pushd /tmp
# Special ydb_routines is needed so that _ydbweburl will be used for the GUI not the one for the performance monitor
ydb_routines="$ydb_dist/plugin/o/_ydbgui.so $ydb_routines"
$ydb_dist/yottadb -run %ydbgui --port 8089 &
popd

term_handler(){
   echo "*** Container stopping - Please wait *****"
   $ydb_dist/yottadb -r %XCMD 'do stop^%zmgsi(0)'
   $ydb_dist/yottadb -run stop^%ydbwebreq --port 9080
   $ydb_dist/yottadb -run stop^%ydbwebreq --port 8089
   $ydb_dist/mupip rundown -r '*'
   exit 0
}

# Setup signal handlers
trap 'term_handler' SIGTERM
trap 'term_handler' SIGINT

wait
