# fastify, stric, Bun, Node, Qoper8-cp + YottaDB

# M/Gateway Developments Ltd
# 2 October 2023

FROM debian:bookworm

# When updating versions of YottaDB and Xider, set the YottaDB major and minor
# versions, and the Xider repo tag here
ARG YDB_MAJOR=r2 YDB_MINOR=02 XIDER_TAG=v0.4.4
ARG YDB_VER=${YDB_MAJOR}${YDB_MINOR}

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y \
  curl \
  build-essential \
  make \
  gcc \
  git \
  gpg \
  wget \
  dos2unix \
  locate \
  nano \
  xinetd \
  libelf1 \
  libtinfo5 \
  lsb-release \
  lsof \
  libssl-dev \
  libcurl4-openssl-dev \
  libicu-dev \
  locales \
  openssh-server \
  subversion \
  pkg-config \
  file \
  python3 \
  apache2 \
  apache2-utils \
  apache2-dev \
  cmake \
  unzip

RUN locale-gen "en_US.UTF-8"

# ===  Install Node.js & NPM

ENV NODE_VERSION=18.19.0

RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    4ED778F539E3634C779C87C6D7062848A1AB005C \
    141F07595B7B3FFE74309A937405533BE57C7D57 \
    74F12602B6F1C4E913FAA37AD3A89613643B6201 \
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7 \
    61FC681DFB92A079F1685E77973F295594EC4689 \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4 \
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
    108F52B48DB57BB0CC439B2997B01419BD92F80A \
  ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  # smoke tests
  && node --version \
  && npm --version

# Install Bun
# RUN curl -fsSL https://bun.sh/install | bash \
RUN curl -fsSL https://bun.sh/install | bash -s -- bun-v1.0.4 \
  && export BUN_INSTALL="$HOME/.bun" \
  && export PATH="$BUN_INSTALL/bin:$PATH"

WORKDIR /opt/qoper8
RUN npm install qoper8-fastify qoper8-stric mg-dbx-napi redis

# Create app directory
RUN mkdir /opt/yottadb \
 && mkdir /opt/mg_web

COPY ./ydb /opt/qoper8
COPY ./ydb_run /opt/qoper8
COPY ./start /opt/qoper8
COPY ./stop /opt/qoper8
COPY ./reconfigure /opt/qoper8
COPY ./build_routes /opt/qoper8
COPY ./restart /opt/qoper8
COPY ./m/ /opt/qoper8/m/
COPY ./gde.txt /opt/qoper8
COPY ./benchmarks/ /opt/qoper8/benchmarks/

RUN echo "Installing YottaDB..."

# Extra prerequisites needed to build YottaDB from source
RUN apt-get install -y --no-install-recommends \
  binutils \
  ca-certificates \
  cmake \
  gawk \
  tcsh \
  libconfig-dev \
  libelf-dev \
  libicu-dev \
  libncurses-dev \
  libreadline-dev

RUN mkdir /tmp/tmp \
  && wget -P /tmp/tmp https://gitlab.com/YottaDB/DB/YDB/raw/master/sr_unix/ydbinstall.sh \
  && cd /tmp/tmp \
  && chmod +x ydbinstall.sh \
  && ./ydbinstall.sh --utf8 default --gui ${YDB_MAJOR}.${YDB_MINOR}

RUN export ydb_gbldir=/opt/yottadb/yottadb.gld \
  && /usr/local/lib/yottadb/$YDB_VER/mumps -run ^GDE < /opt/qoper8/gde.txt \
  && /usr/local/lib/yottadb/$YDB_VER/mupip create \
  && /usr/local/lib/yottadb/$YDB_VER/mupip extend -blocks=48000 DEFAULT \
  && /usr/local/lib/yottadb/$YDB_VER/mupip set -region default -access_method=MM

#################################################
# INSTALLING xider
#################################################
RUN cd /tmp/  && git clone https://gitlab.com/YottaDB/DB/YDBXider.git && cd YDBXider && git checkout $XIDER_TAG && mkdir build && cd build && cmake .. && make && make install

#################################################
# INSTALLING 3n+1
#################################################
RUN apt install -y lua5.1 lua5.1-socket liblua5.1-dev

RUN git clone https://github.com/anet-be/lua-yottadb.git && cd lua-yottadb && make lua=lua5.1 &&  make install lua=lua5.1

RUN cd /tmp/ && git clone https://gitlab.com/YottaDB/Demo/3n-1-ydb.git

ENV LUA_PATH="/tmp/YDBXider/drivers/lua/?.lua;;"

#################################################
# Setting up yottadb
#################################################
ENV ydb_dist="/usr/local/lib/yottadb/$YDB_VER"
ENV ydb_gbldir="/opt/yottadb/yottadb.gld"
ENV ydb_routines="/tmp/YDBXider/routines /tmp/3n-1-ydb /opt/qoper8/m $ydb_dist/plugin/o/_ydbmwebserver.so $ydb_dist/plugin/o/_ydbposix.so $ydb_dist/libyottadbutil.so"
#   /usr/local/lib/yottadb/$YDB_VER/plugin/o/_ydbxider.so
ENV ydb_xc_ydbposix="${ydb_dist}/plugin/ydbposix.xc"
ENV PATH="${PATH}:${ydb_dist}"

# Install and configure the network mgsi interface code to allow
# network access to YottaDB (default setup uses
# API access to YottaDB)

#################################################
# Setting up python yottadb
#################################################
RUN apt install -y python3-dev python3-setuptools python3-venv libffi-dev pip python3-full
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV PYTHONPATH="/tmp/YDBXider/drivers/python"
RUN pip install yottadb redis hiredis

# Install mg-web

RUN cd /opt \
 && git clone https://github.com/chrisemunt/mg_web  \
 && cp /opt/mg_web/src/*.h /opt/mg_web \
 && cp /opt/mg_web/src/*.c /opt/mg_web \
 && cp /opt/mg_web/src/apache/* /opt/mg_web \
 && cd /opt/mg_web \
 && dos2unix *

RUN cd /opt/mg_web \
 && apxs -a -i -c mg_web_apache.c mg_web.c mg_webstatus.c mg_webtls.c mg_websocket.c


# Fetch the generic mg_web server repository
# and move its M code and mg_web conf and log
# files into place

RUN cd /opt \
  && git clone https://github.com/robtweed/mgweb-server \
  && mv /opt/mgweb-server/m/* /opt/qoper8/m/ \
  && mv /opt/mgweb-server/config/mgweb.conf.json /opt/qoper8/mgweb.conf.json \
  && mv /opt/mgweb-server/config/mgweb.log /opt/qoper8 \
  && sed -i 's/x86_64/aarch64/g' /opt/qoper8/mgweb.conf.json \
  && cp /opt/mgweb-server/apache/apache2.conf /etc/apache2 \
  && cp /opt/mgweb-server/apache/ports.conf /etc/apache2 \
  && cp /opt/mgweb-server/apache/mpm_event.conf /opt/mgweb

RUN git clone https://github.com/chrisemunt/mgsi /opt/qoper8/mgsi \
  && cp /opt/qoper8/mgsi/yottadb/* /opt/qoper8/m \
  && yottadb -run ylink^%zmgsi \
  && cp /opt/qoper8/mgsi/unix/zmgsi.ci /usr/local/lib/yottadb/$YDB_VER

RUN cp /opt/qoper8/mgsi/unix/zmgsi_ydb /usr/local/lib/yottadb/$YDB_VER \
  && cp /opt/qoper8/mgsi/unix/zmgsi_xinetd /etc/xinetd.d/zmgsi_xinetd \
  && cp /opt/qoper8/mgsi/unix/zmgsi.ci /usr/local/lib/yottadb/$YDB_VER \
  && sed -i "s/r130/$YDB_VER/g" /etc/xinetd.d/zmgsi_xinetd \
  && sed -i "s/r130/$YDB_VER/g" /usr/local/lib/yottadb/$YDB_VER/zmgsi_ydb \
  && sed -i "s/r1.30/$YDB_MAJOR.$YDB_MINOR/g" /usr/local/lib/yottadb/$YDB_VER/zmgsi_ydb \
  && echo "zmgsi_xinetd          7041/tcp                        # zmgsi" >> /etc/services \
  && rm -r /opt/qoper8/mgsi \
  && rm -r /tmp/tmp

RUN cd /opt/qoper8

RUN npm install nodem \
  && cp /opt/qoper8/node_modules/nodem/src/v4wNode.m /opt/qoper8/m


# Install Redis

RUN curl -fsSL https://packages.redis.io/gpg | gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/redis.list \
  && apt update \
  && apt install -y redis \
  && redis-server --daemonize yes \
  && update-rc.d redis-server defaults

RUN echo 'vm.overcommit_memory = 1' >> /etc/sysctl.conf

#RUN mkdir /redis \
#  && wget http://download.redis.io/redis-stable.tar.gz \
#  && tar xvzf redis-stable.tar.gz \
#  && mv redis-stable /redis \
#  && cd /redis \
#  && make \
#  && make install \

# Configure Redis

#RUN mkdir /etc/redis \
#  && mkdir /var/redis \
#  && cd /redis \
#  && cp utils/redis_init_script /etc/init.d/redis_6379 \
#  && cp redis.conf /etc/redis/6379.conf \
#  && mkdir /var/redis/6379 \
#  && sed -i 's/daemonize no/daemonize yes/g' redis.conf \
#  && sed -i 's[logfile ""[logfile /var/redis/6379/redis_6379.log[g' redis.conf \
#  && sed -i 's[dir ./[dir /var/redis/6379[g' redis.conf \
#  && update-rc.d redis_6379 defaults

# Clean up

EXPOSE 8089 8090 9080

RUN rm gde.txt

RUN mkdir /opt/qoper8/js /opt/qoper8/img /opt/qoper8/libs
COPY ./index.html /opt/qoper8/index.html
COPY css/perf.css /opt/qoper8/css/perf.css
COPY ./img/* /opt/qoper8/img/
COPY js/* /opt/qoper8/js/
COPY html/* /opt/qoper8/html/
COPY ./fonts /opt/qoper8/fonts/
COPY ./libs/ /opt/qoper8/libs/
COPY start2.sh /start

SHELL ["/bin/bash", "-c"]
ENTRYPOINT /start

# to build the image: docker build --no-cache -t ydb-perf .
# to run the image: docker run -p 8089-8090:8089-8090 -p 9080:9080 --rm --name=ydb-perf -it -d ydb-perf

# to run the docker compose (and mount the dev directories):
# docker compose up  -d --build --force-recreate
