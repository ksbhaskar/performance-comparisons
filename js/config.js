const app = {}

// in order to add new languages, you can add entries to the following "languages" array
// the "commandLine" property is the command line to be executed. The correct path will be automatically set by the M code
// the "sourceCode" property is the absolute path of the source code
// The chart.name is the text displayed in the chart, for the series.
// The useRedis flag is used to instruct the parser to process only "embedded" test nodes.

/*

To create a node.js entry, add this to the "languages" array and fill the empty strings with correct data:
{
    displayName: 'Node.js',
    useRedis: true,
    tests: {
        tcp: [
            {
                database: 'Redis',
                commandLine: '',
                sourceCode: '',
                chart: {
                    name: 'Redis - node.js driver'
                }
            },
            {
                database: 'Xider',
                commandLine: '',
                sourceCode: '',
                chart: {
                    name: 'Xider - node.js driver'
                }
            }
        ],
            direct: [
            {
                database: 'Xider',
                commandLine: '',
                sourceCode: '',
                chart: {
                    name: 'Xider - node.js in-process'
                }
            },
            {
                database: 'YottaDB',
                commandLine: '',
                sourceCode: '',
                chart: {
                    name: 'YottaDB - node.js in-process'
                }
            }
        ]
    }
},
*/



app.config = {
    languages: [
        {
            displayName: 'Lua',
            useRedis: true,
            tests: {
                tcp: [
                    {
                        database: 'Redis',
                        commandLine: 'PORT=6379 ./redis3n1.lua',
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.lua',
                        chart: {
                            name: 'Redis - LUA driver'
                        }
                    },
                    {
                        database: 'Xider',
                        commandLine: 'PORT=3000 ./redis3n1.lua',
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.lua',
                        chart: {
                            name: 'Xider - LUA driver'
                        }
                    }
                ],
                direct: [
                    {
                        database: 'Xider',
                        commandLine: './redis3n1.lua --xider',
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.lua',
                        chart: {
                            name: 'Xider - Lua in-process'
                        }
                    },
                    {
                        database: 'YottaDB',
                        commandLine: './ydb3n1.lua ',
                        sourceCode: '/tmp/3n-1-ydb/ydb3n1.lua',
                        chart: {
                            name: 'YottaDB - Lua in-process'
                        }
                    }
                ]
            }
        },
        {
            displayName: 'Python',
            useRedis: true,
            tests: {
                tcp: [
                    {
                        database: 'Redis',
                        commandLine: 'PORT=6379 ./redis3n1.py',
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.py',
                        chart: {
                            name: 'Redis- Python driver'
                        }
                    },
                    {
                        database: 'Xider',
                        commandLine: 'PORT=3000 ./redis3n1.py',
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.py',
                        chart: {
                            name: 'Xider - Python driver'
                        }
                    }
                ],
                direct: [
                    {
                        database: 'Xider',
                        commandLine: './redis3n1.py --xider', //
                        sourceCode: '/tmp/3n-1-ydb/redis3n1.py',
                        chart: {
                            name: 'Xider - Python in-process'
                        }
                    },
                    {
                        database: 'YottaDB',
                        commandLine: './ydb3n1.py',
                        sourceCode: '/tmp/3n-1-ydb/ydb3n1.py',
                        chart: {
                            name: 'YottaDB - Python in-process'
                        }
                    }
                ]
            }
        },
        {
            displayName: 'M',
            useRedis: false,
            tests: {
                embedded:
                    {
                        database: 'YottaDB',
                        commandLine: 'ydb -run ydb3n1 ',
                        sourceCode: '/tmp/3n-1-ydb/ydb3n1.m',
                        chart: {
                            name: 'YottaDB embedded'
                        }
                    },
            }
        },
    ]
}
