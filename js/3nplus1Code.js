app.threenplusone.code = {
    init: function () {
        $('#btnCodeToClipboard').on('click', () => this.clipboardClicked())
    },

    show: async function (file) {
        let mode

        // fetch the file
        let ret = await app.threenplusone.run.getFile(file)
        this.code = ret

        // load the file into the text area
        $('#divCodeViewer').empty()

        // find mode
        switch (file.split('.').pop()) {
            case 'py': {
                mode = 'python'

                break
            }
            case 'lua': {
                mode = 'lua'

                break
            }
            case 'm': {
                mode = 'mumps'
            }
        }

        // initialize the parser
        this.cm = CodeMirror(document.getElementById('divCodeViewer'), {
            value: ret,
            lineNumbers: true,
            lineWrapping: false,
            readOnly: true,
            mode: mode,
            theme: 'default', //'night'
        });

        $('.CodeMirror').css('height', '500px')

        $('#lblCodeTitle').text(file)

        // display the dialog
        $('#modalCode')
            .modal({show: true, backdrop: 'static'})
            //.draggable({handle: '.modal-header'})
            .on('shown.bs.modal', () => {
                // refresh the viewer
                this.cm.refresh();
                $('.CodeMirror').focus()
            })
    },

    clipboardClicked: function () {
        copyToClipboard(this.code, $('#btnCodeToClipboard'))
    },

    cm: {},
    code: ''
}
