app.threenplusone.chart = {
    init: function () {

    },

    show: function () {
        let series = []

        app.threenplusone.results.jobs.forEach(result => {
            if (result.enabled === true && !result.aborted) {
                if (result.final) {
                    series.push({
                        name: result.chart.name,
                        data: [result.final.duration]
                    })
                }
            }

        })

        let options = {
            series: series,
            chart: {
                type: 'bar',
                height: 500
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '80%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            title: {
                text: '3n+1 speed comparison - Size: ' + app.formatThousands(app.threenplusone.results.params.inputSize) + ' / Workers: ' + app.threenplusone.results.params.workers
            },
            legend: {
                fontWeight: 'bold',
            },
            /*
            annotations: {
                points: [{
                    x: 'Speed',
                    y: 1,
                    seriesIndex: 0,
                    marker: {
                        offsetX: -20
                    },
                    label: {
                        borderColor: '#775DD0',
                        offsetY: 0,
                        style: {
                            color: '#fff',
                            background: '#775DD0',
                        },
                        text: 'Bananas are good',
                    }
                }]
            },

             */
            xaxis: {
                type: 'category',
                categories: [''],
            },
            yaxis: {
                title: {
                    text: 'Speed (seconds)'
                },
                forceNiceScale: true,
                stepSize: 0.1,
                tickAmount: 10
            },
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val + ' seconds'
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "vertical",
                    shadeIntensity: 0.5,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 50, 100],
                    colorStops: []
                }
            }
        };

        let chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();

        // display the dialog
        $('#modalChart')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    }
}
