window.onload = async () => {
	// ********************************
	// loads all included HTML
	// ********************************
	await loadFiles('data-include');

	populatePre('mgx', 'mgx-pre', '/node/mgdbx');
	populatePre('redis-par', 'redis-par-pre', '/node/redis2');
	populatePre('redis-ser', 'redis-ser-pre', '/node/redis1');
	populatePre('nodem', 'nodem-pre', '/node/nodem');
	populatePre('mgxnw', 'mgxnw-pre', '/node/mgdbx-nw');

	app.threenplusone.init()
	app.threenplusone.run.init()
	app.threenplusone.code.init()
	app.threenplusone.table.populate()
}

function populatePre(buttonId, preId, endpoint) {
    document.getElementById(buttonId).addEventListener('click', () => {
        document.getElementById(preId).textContent = "fetching " + endpoint;

        fetch(endpoint).then(response => {
	    if (response.ok) {
		    return response.text();
		} else {
			return "error from web service call"
		}
		}).then(data => {
			document.getElementById(preId).textContent = data;
		})
	})
}

const loadFiles = async (tag) => {
	let promises = [];

	let includes = $('[' + tag + ']');
	jQuery.each(includes, async function () {
		let def = new $.Deferred();

		const newThis = this;
		let file = './html/' + $(newThis).data('include') + '.html';
		$(this).load(file, () => {
			def.resolve();
		});
		promises.push(def)
	});

	return $.when.apply(undefined, promises).promise();
};

const copyToClipboard = (clipboardText, $button = null) => {
	if (location.protocol === 'http:') {
		navigator.clipboard.writeText(clipboardText)

		if ($button !== null) {
			const oldText = $button.html()
			$button.html('Copied to clipboard')

			setTimeout(() => {
				$button.html(oldText)

			}, 1500, $button, oldText)
		} else {
			app.ui.msgbox.show('The text has been copied to the clipboard...', 'Notification')
		}

	} else {
		alert(clipboardText)
	}
}
