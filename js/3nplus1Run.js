app.threenplusone.run = {
    init: function () {
        $('#btnRunAbort').on('click', () => this.closeButtonClicked())
    },

    show: async function (params) {
        let progressWidth = 0
        let jobs = []
        const progRun = $('#progRun')
        const lblRunCurrentNumber = $('#lblRunCurrentNumber')
        const lblRunCurrentScript = $('#lblRunCurrentScript')
        const btnRunAbort = $('#btnRunAbort')

        console.log(params)

        // determine how many jobs have to run
        params.jobs.forEach((job, ix) => {
            if (job.enabled === true && job.commandLine !== '') jobs.push(job)
        })

        this.clearRunningLines(jobs)

        // init the entries
        progRun.css('width', '0')
        $('#lblRunTotal').text(jobs.length)

        app.button.disable(btnRunAbort)

        // display the dialog
        $('#modalRun')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})

        // loop through the items
        for (let ix = 0; ix < jobs.length; ix++) {
            lblRunCurrentNumber.text(ix + 1)
            lblRunCurrentScript.text(jobs[ix].commandLine + ' ' + params.inputSize + ' ' + params.workers)
            progressWidth += (100 / jobs.length)

            // starts the Xider server if needed
            if (jobs[ix].commandLine.indexOf('=3000') > -1 || jobs[ix].commandLine.indexOf('--xider') > -1) {
                console.log('starting server')
                // start server
                this.xiderPid = await this.startXiderServer()
                console.log('Using PID ' + this.xiderPid)
            }

            console.log('Executing: ' + jobs[ix].commandLine + ' ' + params.inputSize + ' ' + params.workers)

            app.button.enable(btnRunAbort)
            const result = await this.executeTask('/threen/exec-script?line=' + jobs[ix].commandLine + ' ' + params.inputSize + ' ' + params.workers)
            app.button.disable(btnRunAbort)
            jobs[ix].result = result.split('\n')

            // stops the Xider server if needed
            if (jobs[ix].commandLine.indexOf('=3000') > -1 || jobs[ix].commandLine.indexOf('--xider') > -1) {
                console.log('stopping server')
                // stop server
                await this.stopXiderServer()
            }

            const final = []

            // extract the result
            const job = jobs[ix]

            delete job.aborted
            if (job.result[0].length > 50 && job.result && job.result[1] === '' && job.result[2] === '' && job.result[0].indexOf('1->') > -1) {
                let split = job.result[0].split(' ')
                split.forEach(res => {
                    final.push(res.split('='))
                })

                job.final = {
                    duration: final[5][1].replace(',', ''),
                    updates: final[8][1].replace(',', ''),
                    reads: final[9][1].replace(',', '')
                }

                // update the table
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > span').text(job.enabled === true ? final[5][1] : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 1) + ')').text(job.enabled === true ? final[3][1] : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 2) + ')').text(job.enabled === true ? final[4][1] : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 3) + ')').text(job.enabled === true ? final[6][1] : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 4) + ')').text(job.enabled === true ? final[7][1] : '')

            } else if (job.result[0].length < 50 && job.result.length === 3) {
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > span').text(job.enabled === true ? 'Aborted' : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > div > div').css('width', '0%')

                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 1) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 2) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 3) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 4) + ')').text('')
                job.aborted = true

            } else {
                // errors
                console.log(job.result)
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > span').text(job.enabled === true ? 'Errors' : '')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > div > div').css('width', '0%')

                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 1) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 2) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 3) + ')').text('')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 4) + ')').text('')
                job.aborted = true
            }

            progRun.css('width', progressWidth + '%')
        }

        // find the highest value
        let maxDuration = 0
        jobs.forEach(job => {
            if (job.final && parseFloat(job.final.duration) > maxDuration) maxDuration = parseFloat(job.final.duration)
        })

        jobs.forEach(job => {
            if (job.final && !(job.result[0].length < 50 && job.result.length === 3)) {
                const percent = job.final.duration / maxDuration * 100
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > div > div').css('width', percent + '%')
            }
        })

        app.threenplusone.results = {
            params: params,
            jobs: jobs
        }

        // enable the chart button
        app.button.enable($('#btnThreeNChart'))

        // close dialog
        setTimeout(() => {
            $('#modalRun').modal('hide')

        }, 500)

        //app.threenplusone.chart.show()
    },

    closeButtonClicked: function () {
        this.abortProcess()
    },

    clearRunningLines: function (jobs) {
        jobs.forEach(job => {
            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > span').text('In progress...')
            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + job.td + ') > div > div').css('width', '0%')

            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 1) + ')').text('')
            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 2) + ')').text('')
            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 3) + ')').text('')
            $('#tbl3nplus1 > tbody > tr:nth-child(' + (job.row + 1) + ') > td:nth-child(' + (job.td + 4) + ')').text('')
        })
    },

    executeTask: async function (endpoint) {
        return new Promise(function (resolve, reject) {
            fetch(endpoint).then(response => {
                if (response.ok) {
                    return response.text();
                } else {
                    console.log(response)
                    return "error from web service call"
                }
            }).then(data => {
                resolve(data)
            })
        })
    },

    delay: async function (time) {
        return new Promise(function (resolve) {
            setTimeout(resolve, time);
        });
    },

    startXiderServer: async function () {
        const res = await this.executeTask('/threen/start-xider')
        const split = res.split('\n')

        return split[1].split(' ')[3].split('=')[1].slice(0, -1)
    },

    stopXiderServer: async function () {
        const res = await this.executeTask('/threen/stop-xider?pid=' + this.xiderPid)
    },

    abortProcess: async function () {
        const res = await this.executeTask('/threen/abort')
    },

    getFile: async function (filename) {
        return await this.executeTask('/threen/get-file?filename=' + filename)
    },

    xiderPid: ''
}
