app.threenplusone = {
    init: function () {
        $('#btnThreeNRun').on('click', () => this.runPressed())
        $('#btnThreeNChart').on('click', () => this.chartPressed())
        $('#chkThreeNAll').on('click', () => this.table.checkSelectAll())
    },

    table: {
        populate: function () {
            const tbl3nplus1 = $("#tbl3nplus1 > tbody")
            let rowCounter = 0
            let rows = ''

            tbl3nplus1.empty()

            app.config.languages.forEach(language => {

                    if (language.useRedis === true) {
                        // FIRST the TCP
                        rowCounter += 1
                        rows += '<tr>'

                        rows += '<td rowspan="4" style="vertical-align: middle;"><input data-language="' + language.displayName + '" type="checkbox" class="check-boxes" id="chkThreeN-' + language.displayName + '" onclick="app.threenplusone.table.checkPressed(this, ' + rowCounter + ')"></td>'

                        rows += '<td rowspan="4" style="vertical-align: middle;">' + language.displayName + '</td>'
                        rows += '<td rowspan="2" style="vertical-align: middle;">TCP</td>'
                        rows += '<td>' + language.tests.tcp[0].database + '</td>'
                        rows += '<td> <span class="threenplus1-table-link" onclick="app.threenplusone.code.show(\'' + language.tests.tcp[0].sourceCode + '\')">' + language.tests.tcp[0].commandLine + '</span></td>'

                        rows = this.appendEmptyResult(rows, language.tests.tcp[0].commandLine !== '', language.tests.tcp[0].database)

                        rows += '</tr>'

                        app.threenplusone.jobs.push({
                            commandLine: language.tests.tcp[0].commandLine,
                            td: 6,
                            chart: language.tests.tcp[0].chart
                        })

                        rowCounter += 1
                        rows += '<tr >'

                        rows += '<td>' + language.tests.tcp[1].database + '</td>'
                        rows += '<td> <span class="threenplus1-table-link" onclick="app.threenplusone.code.show(\'' + language.tests.tcp[1].sourceCode + '\')">' + language.tests.tcp[1].commandLine + '</span></td>'

                        rows = this.appendEmptyResult(rows, language.tests.tcp[1].commandLine !== '', language.tests.tcp[1].database)

                        rows += '</tr>'
                        app.threenplusone.jobs.push({
                            commandLine: language.tests.tcp[1].commandLine,
                            td: 3,
                            chart: language.tests.tcp[1].chart
                        })

                        // then the direct
                        let test = language.tests.direct[0]
                        rowCounter += 1
                        rows += '<tr>'

                        rows += '<td rowspan="2" style="vertical-align: middle;">DIRECT</td>'
                        rows += '<td>' + test.database + '</td>'
                        rows += '<td> <span class="threenplus1-table-link" onclick="app.threenplusone.code.show(\'' + test.sourceCode + '\')">' + test.commandLine + '</span></td>'

                        rows = this.appendEmptyResult(rows, test.commandLine !== '', test.database)

                        rows += '</tr>'
                        app.threenplusone.jobs.push({
                            commandLine: test.commandLine,
                            td: 4,
                            chart: test.chart
                        })

                        test = language.tests.direct[1]

                        rowCounter += 1
                        rows += '<tr>'

                        rows += '<td>' + test.database + '</td>'
                        rows += '<td> <span class="threenplus1-table-link" onclick="app.threenplusone.code.show(\'' + test.sourceCode + '\')">' + test.commandLine + '</span></td>'

                        rows = this.appendEmptyResult(rows, test.commandLine !== '', test.database)

                        rows += '</tr>'
                        app.threenplusone.jobs.push({
                            commandLine: test.commandLine,
                            td: 3,
                            chart: test.chart
                        })

                    } else {

                        // YDB
                        rowCounter += 1
                        rows += '<tr>'

                        rows += '<td ><input type="checkbox" class="check-boxes" data-language="' + language.displayName + '" id="chkThreeN-' + language.displayName + '"  onclick="app.threenplusone.table.checkPressed(this, ' + rowCounter + ')"></td>'

                        rows += '<td >' + language.displayName + '</td>'
                        rows += '<td >Embedded</td>'
                        rows += '<td>' + language.tests.embedded.database + '</td>'
                        rows += '<td> <span class="threenplus1-table-link" onclick="app.threenplusone.code.show(\'' + language.tests.embedded.sourceCode + '\')">' + language.tests.embedded.commandLine + '</span></td>'

                        rows = this.appendEmptyResult(rows, language.tests.embedded.commandLine !== '', language.tests.embedded.database)

                        rows += '</tr>'
                        app.threenplusone.jobs.push({
                            commandLine: language.tests.embedded.commandLine,
                            td: 6,
                            chart: language.tests.embedded.chart
                        })
                    }
                }
            )

            tbl3nplus1.append(rows)

            $('#tbl3nplus1 > tbody > tr').each((row, tr) => {
                tr.style.backgroundColor = 'white'
            })
            $('#chkThreeNAll').prop('checked', true)

            this.checkSelectAll()
        },

        appendEmptyResult: function (rows, withPercent, db) {
            rows += '<td>' + (withPercent === true ? '<span>&nbsp;</span><div class="progress" ><div class="progress-bar ' + (db === 'Redis' ? 'bg-ydb-red' : 'bg-ydb-green') + '" role="progressbar" style="width: 0%;"></div></div>' : '') + '</td>'
            rows += '<td></td>'
            rows += '<td></td>'
            rows += '<td></td>'
            rows += '<td></td>'

            return rows
        },

        checkSelectAll: function () {
            const checked = $('#chkThreeNAll').is(':checked')

            $('#tbl3nplus1 > tbody > tr').css('background', checked === true ? 'white' : 'lightgray')

            $('.check-boxes').prop('checked', checked)

            app.button.set($('#btnThreeNRun'), checked === true ? app.button.ENABLED : app.button.DISABLED)
        },

        checkPressed: function (that, rowCounter) {
            const check = $('#' + that.id)
            const checked = check.is(':checked')
            const language = check.data('language')

            if (language === 'M') {
                $('#tbl3nplus1 > tbody > tr:nth-child(' + rowCounter + ')').css('background', checked === true ? 'white' : 'lightgray')

            } else {
                $('#tbl3nplus1 > tbody > tr:nth-child(' + rowCounter + ')').css('background', checked === true ? 'white' : 'lightgray')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (rowCounter + 1) + ')').css('background', checked === true ? 'white' : 'lightgray')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (rowCounter + 2) + ')').css('background', checked === true ? 'white' : 'lightgray')
                $('#tbl3nplus1 > tbody > tr:nth-child(' + (rowCounter + 3) + ')').css('background', checked === true ? 'white' : 'lightgray')
            }

            if (checked === true) {
                app.button.enable($('#btnThreeNRun'))

            } else {
                // check if all are disabled and disable the run button if so
                let allUnchecked = true

                $('.check-boxes').each((ix, el) => {
                    if ($(el).is(':checked') === true) {
                        allUnchecked = false
                    }
                })

                app.button.set($('#btnThreeNRun'), allUnchecked === false ? app.button.ENABLED : app.button.DISABLED)
            }
        },
    },

    runPressed: function () {
        let executeParams = {
            inputSize: $('#selThreeNSize').find(':selected').val(),
            workers: $('#selThreeNWorkers').find(':selected').val(),
            jobs: this.jobs,
        }

        // determine which jobs must run
        $('#tbl3nplus1 > tbody > tr').each((row, tr) => {
            executeParams.jobs[row].enabled = tr.style.backgroundColor === 'white'
            executeParams.jobs[row].row = row
        })

        app.threenplusone.run.show(executeParams)
    },

    chartPressed: function () {
        app.threenplusone.chart.show()
    },

    jobs: [],

    results: {}
}

// enable / disable buttons
app.button = {
    enable: function ($button) {
        $button
            .removeClass('disabled')
            .attr('disabled', false)
    },

    disable: function ($button) {
        $button
            .addClass('disabled')
            .attr('disabled', true)

    },

    set: function ($button, status) {
        switch (status) {
            case this.ENABLED:
                this.enable($button);
                break
            case this.DISABLED:
                this.disable($button);
                break
            default:
                return
        }
    },

    ENABLED: 'enabled',
    DISABLED: 'disabled'
}

app.formatThousands = x => {
    if (isNaN(x) === true && x !== 0) return x

    //inserts a comma as thousands separator each 3 chars
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
};

